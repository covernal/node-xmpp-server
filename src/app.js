"use strict";

var GoChat = require( "../lib/index" ),
    Element = require( "node-xmpp-core" ).ltx.Element

var program = require('commander');

program
  .version('0.0.1')
  .option('-H, --host [host]', 'Set hostname', '192.168.1.40')
  .parse(process.argv);

console.log('+-----------------------------------------------------------+');
console.log('| goOnChatServer start with the host name of %s.   |', program.host);
console.log('+-----------------------------------------------------------+');

var host = program.host;

GoChat.Server.prototype.initUserList = function() {
    console.log("Server inited user list");
    var list = [];


    var echo_friend = new GoChat.User( "echo@"+host, "EchoBot" );

    echo_friend.onRecieveMessage = function( message ) {
        console.log(this.jid.getLocal()+ ': onRecieveMessage : ' + message.toString());
        if( message.getChild("body") ) {
            console.log(this.jid.getLocal()+ ': onRecieveMessage : echo : ' + message.getChild("body").getText());
            echo_friend.sendMessageFrom( new GoChat.JID( message.attrs.from ), message.getChild("body").getText() );
        }

      // if( message.getChild( "body" ) ) {
      //     var text = message.getChild( "body" ).getText();
      //     //var toUser = new GoChat.JID( message.attrs.from.toString() );

      //     var item = new Element( 'message', { to: message.attrs.from.toString(), // Strip out resource
      //                                           from: this.jid.getLocal() + "@" + this.jid.getDomain(),
      //                                           type: "chat",
      //                                           id: "JabberServer" } )
      //                    .c("body").t( message );
      //     this.client.emit('message', item);
      // }        
    }

    var user1 = new GoChat.User( "client1@"+host, "Vernal" );
    var user2 = new GoChat.User( "client2@"+host, "Passion" );
    var user3 = new GoChat.User( "client3@"+host, "Bjorn" );

    user2.addBuddy( echo_friend );
    user2.addBuddy( user1 );
    user2.addBuddy( user3 );

    user1.addBuddy( user2 );
    user1.addBuddy( user3 );
    user1.addBuddy( echo_friend );

    user3.addBuddy( echo_friend );
    user3.addBuddy( user1 );
    user3.addBuddy( user2 );

    list.push( user1 );
    list.push( user2 );
    list.push( user3 );
    list.push( echo_friend );
    return list;
}

var server = new GoChat.Server( { domain: host, port: 5222, tls: false } );

console.log("Server started");