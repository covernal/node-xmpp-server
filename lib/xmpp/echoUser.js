'use strict';

var User = require( "./user" ),
	util = require( "util" )

function EchoUser( jid, name ) {
    User.call(this, jid, name)
}

util.inherits( EchoUser, User );

EchoUser.prototype.onRecieveMessage = function( message ) {
	console.log(this.jid.getLocal()+ ': onRecieveMessage');

    if( message.getChild( "body" ) ) {
        var text = message.getChild( "body" ).getText();
        var toUser = new JID( message.attrs.from.toString() );

		var item = new Element( 'message', { from: message.attrs.from.toString(), // Strip out resource
                                              to: this.jid.getLocal() + "@" + this.jid.getDomain(),
                                              type: "chat",
                                              id: "JabberServer" } )
                       .c("body").t( message );
        this.client.emit('message', item);
    }
};

module.exports = EchoUser;