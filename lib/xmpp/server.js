'use strict';

var xmpp = require("node-xmpp-server"),
    User = require("./user"),
    EventEmitter = require( "events" ).EventEmitter,
    util = require( "util" ),
    Element = require( "node-xmpp-core" ).ltx.Element,
    JID = require( "node-xmpp-core" ).JID

var JABBER_INFO = "http://jabber.org/protocol/disco#info"
var JABBER_ITEMS = "http://jabber.org/protocol/disco#items"
var JABBER_ROSTER = "jabber:iq:roster"

function Server( opts, callback ) {
    this.opts = {};
    if( opts ) this.opts = opts;
    this.base = xmpp.C2SServer;
    this.base( opts );

    this.userlist = {};
    var nonindexedlist = this.initUserList();

    for( var user in nonindexedlist ) {
        this.userlist[ nonindexedlist[user].getKey() ] = nonindexedlist[user];
        nonindexedlist[user].messageCallback = this.sendMessage.bind( this )
    }

    this.on( 'online', function() {
        if( callback )
            callback();
    } );

    this.on( 'shutdown', function() {
        for( var user in this.userlist ) {
            this.userlist[ user ].sendShutdownToStream();
        }
        this.emit( this, 'shutdown' );
    } );
    this._addConnectionListener(); // For some reason if we add this it get's called twice
};

util.inherits( Server, xmpp.C2SServer );

Server.prototype._addConnectionListener = function() {
    var self = this;
    console.log("_addConnectionListener");
    this.on( 'connect', function( session ) {
        console.log("connect")
        self._clientConnected( session )
    });
}

Server.prototype._getOnlineUsers = function() {
    var onlineusers = []
    for( var user in this.userlist ) {
        if( user.online ) {
            onlineusers.push( user );
        }
    }
}

// session -> node-xmpp-server/c2s/Session.js
// session object has created in acceptConnection() at node-xmpp-server/Server.js
Server.prototype._clientConnected = function( session ) {
    var jabberserver = this

    console.log("_clientConnected");

    session.on('register', function( opts, cb ) {
        var user = new User( opts.jid.toString(), opts.jid.toString() );
        jabberserver.addUser( user );

        this.emit( 'registration-success', user );
    });

    session.on('authenticate', function( opts, cb ) {
        //console.log('server:', opts.username, opts.password, 'AUTHENTICATING')
        console.log( "User " + opts.jid.bare() + " reqested login" );
        if( jabberserver.userlist[ opts.jid.bare() ] != null ) {
            var auth_success = jabberserver.userlist[ opts.jid.bare() ].authenticate( opts );

            cb(!auth_success, opts);
        } else {
            console.log( "User " + opts.jid.bare() + " not found" );
            cb(true, opts);
        }
    });

    session.on('auth-success', function( jid ) {
        jabberserver.userlist[ jid.bare() ].stream = session;
        // jabberserver.userlist[ jid.bare() ].messageCallback = this.sendMessage
    });

    session.on( 'stanza', function( stanza ) {
        //console.log( "onStanza" );
        //console.log('server:', session.jid.local, 'stanza', stanza.toString());
        if( stanza.getChild('query') ) { // Info query
            this.emit( 'query', stanza );
        } else if( stanza.getName() == "presence" ) { // Presence
            this.emit( 'presence', stanza );
        } else if( stanza.getName() == "message" ) { // Message
            this.emit( 'message', stanza );
        } else if( stanza.getChild( 'ping' ) != null ) {
            this.emit( 'ping', stanza );
        } else {
            console.log("Stanza Not Identified " + stanza);
        }
    });

    session.on( 'query', function( query ) {
        var type = query.getChild('query').attrs.xmlns
        var result = new Element( 'iq', { type:       'result',
                                          from:       query.attrs.to,
                                          to:         query.attrs.from,
                                          id:         query.attrs.id } )
                             .c( "query", { xmlns: type } )

        this.emit( type, query, result )
        session.send( result )
    });

    session.on( JABBER_ITEMS, function( query, parent ) {
        parent.c( "account", { type: "registered" } );
    } );

    session.on( JABBER_INFO, function( query, parent ) {
        parent.c( "account", { type: "registered" } );
    } );

    session.on( JABBER_ROSTER, function( query, parent ) {
        var jid = new JID( query.attrs.from.toString() );

        if( jabberserver.userlist[ jid.bare() ] ) {
            jabberserver.userlist[ jid.bare() ].updateFriends( parent )
        } else {
            console.log("No user to roster update");
        }
    } );

    session.on( 'presence', function( query ) {
        var jid = new JID( query.attrs.from.toString() );

        if( jabberserver.userlist[ jid.bare() ] ) {
            jabberserver.userlist[ jid.bare() ].updatePresence( session );
        } else {
            console.log("No user to roster update");
        }
    } );

    session.on( 'message', function( query ) {
        console.log(this.jid.getLocal()+ ": message emited" + query.toString());
        var jid = new JID( query.attrs.to.toString() );

        if( jabberserver.userlist[ jid.bare() ] ) {
            jabberserver.userlist[ jid.bare() ].sendMessageToStream( query );
        }
    });

    session.on( 'ping', function( query ) {
        var jid = new JID( query.attrs.from.toString() );

        if( jabberserver.userlist[ jid.bare() ] ) {
            jabberserver.userlist[ jid.bare() ].respondToPing( query );
        }
    } );
}

Server.prototype.sendMessage = function( to, from, message ) {
    if( this.userlist[ to.bare() ] && this.userlist[ from.bare() ] ) {
        this.userlist[ to.bare() ].sendMessageTo( this.userlist[ from.bare() ], message );
    }
}

Server.prototype.initUserList = function() {
    return {};
}

Server.prototype.addUser = function( user ) {
    this.userlist[ user.getKey() ] = user;
}

module.exports = Server;
