'use strict';


module.exports = {
    User: require( "./xmpp/user" ),
    EchoUser: require( "./xmpp/echoUser" ),

    BuddyList: require( "./xmpp/buddylist" ),
    Server: require( "./xmpp/server" ),
    JID: require( "node-xmpp-core" ).JID
}
